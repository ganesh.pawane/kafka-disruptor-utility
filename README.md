# Kafka-Disruptor-Utility

Problem statement : Generic kafka-Disruptor Queue  pipeline. 
Abstract: The basic Idea is to build full fledged queuing mechanism which can store data in Disruptor queue published by Kafka server. This library would explore processing methods just providing Inbound and Outbound details. This library would provide following ways of storage.

1. Kafka to Disruptor Queue
2. Disruptor Queue to Kafka
3. Disruptor Queue to Disruptor Queue
4. Kafka to Kafka
